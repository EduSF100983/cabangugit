<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */
@ini_set( 'upload_max_filesize' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'memory_limit', '256M' );
@ini_set( 'max_execution_time', '300' );
@ini_set( 'max_input_time', '300' );
// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'cabangu' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'pr(!Skj0gm7Mo5Vuy(*IA8@Xh6@%Vi*mx[<E??M=sLzC%AOxR1B4Ioo}0-A?3(o;' );
define( 'SECURE_AUTH_KEY',  '[Y.+jSjVuz}/YQ-Jxx<?f{x+kX0X40CjpMA)ZxG|XPfQxw,7{Kx,,@$B:HoZgH0q' );
define( 'LOGGED_IN_KEY',    ';i&~G$HP8wS_UNn;O`m&<Dri.?JR^0L}S9b#wjOJ~L6/7(_;11OPMaQXvGtQuQ,(' );
define( 'NONCE_KEY',        '3b0uUnN;EWFTm].Qs7-25dzY}|ftIL7;xyzL<Ht*_d_=nUaX7L{qZBWuWUN1MR4l' );
define( 'AUTH_SALT',        '>A]@_:XX,d)yzG42,5e%J# jN[Z%kr|~UON~/=fG#bp-#guL,ZKpf]8I@7$!En} ' );
define( 'SECURE_AUTH_SALT', '-z!~1i@?p7U7hb ^[OK<:Pt`$gZaz:hp]^B)`Yx%#Cy*D$n[`@!_9Up7T#sec6W@' );
define( 'LOGGED_IN_SALT',   'HW3X(m.&Tk:N% pi6Q^;CSU0.g+NINg7prs|T:4(bljy.s5^%);G):30;1U>&ht9' );
define( 'NONCE_SALT',       'TDA8OSv1wXA_0;|MhoVu,6ukSHf|R|10j^xNoZJI6RpK24/1j[ogGBNPMxx!KlDN' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
