<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cabangu
 */

get_header();
$mypost = get_page_by_path('Suporte', '', 'page');
?>
<section class="como-podemos-ajudar  bg-gray-light">
    <div class="container">
      <div class="row ">
        <div class="col-sm ">
          <div class="col-header  text-center">
            <h2 class="my-0 text-blue font-weight-900">Como podemos ajudar?</h2>
          </div>
          <div class="col-body text-center text-gray mt50" style="font-size: 22px;  ">
            <p><?php echo get_field('como_podemos_ajudar',  $mypost->ID); ?> </p>
          </div>


          <div class="col-body-cards text-center text-gray mt50" style="font-size: 22px;">
           	<div class="row especial-padding" style="">
           		<div class=" card col-sm  ">
	           		<div class=" text-center  " >
	           			<h3>Suporte  <br>Remoto</h3>
	           			<div class="icone-centro text-center">
	           				<span class="centralizado fone-ico" ></span>
	           			</div>
	           			<p><?php echo get_field('suporte_remoto',  $mypost->ID); ?> </p>
	           		</div>
           		</div>
           		<div class="card col-sm">
	           		<div class="text-center  ">
	           			<h3>Teste de velocidade</h3>
	           			<div class="icone-centro text-center">
	           				<span class="centralizado radio-ico" ></span>
	           			</div>
	           			<p><?php echo get_field('teste_velocidade',  $mypost->ID); ?> </p>
	           		</div>
           		</div>
           		<div class="card col-sm">
	           		<div class="text-center  ">
	           			<h3>Parceria de vendas</h3>
	           			<div class="icone-centro text-center">
	           				<span class="centralizado grana-ico" ></span>
	           			</div>
	           			<p><?php echo get_field('parceria_de_vendas',  $mypost->ID); ?> </p>
	           		</div>
           		</div>
           	</div>
          </div>
        </div>
      </div>
    </div>
 </section>
 <section id="duvidas-frequentes"  class="duvidas-frequentes bg-white">
    <div class="container">
      <div class="row ">
        <div class="col-sm ">
          <div class="col-header  text-left">
            <h1 class="my-0 text-blue font-weight-900">Dúvidas Frenquentes</h2>
          </div>

        </div>
      </div>
	  <div class="row mt50">
			
		<div class="col-sm-3 ">
			<h2 class="my-0 text-gray font-weight-900">Internet</h2>

			<h4><a href="">Fibra Ótica</a></h4> 
			<h4><a href="">Corporativa</a></h4>
			<h4><a href="">Radio</a></h4> 
		</div>
		<div class="col-sm-9 ">

			 <ul class="faq">  
			 <?php 
$args = array( 
'orderby' => 'id',
'order'   => 'ASC',
'post_type' => 'faq',
'posts_per_page' => -1
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<li>  <div class="pergunta text-bold"><?php the_title() ;?>  <span class="sinal ">+</span></div> <div class="resposta" style="display: none;"><?php the_content() ?></div></li>
    
<?php endwhile; else: ?> <p>Não existe perguntas pra exibir!</p> <?php endif; ?>
  </ul>  
<?php wp_reset_query(); ?>
          </ul>  
		</div>
	  </div>
    </div>
 </section>
 <section id="area-cobertura" class="contato-suporte bg-white">
    <div class="container">
      <div class="row ">
        <div class="col-sm ">
          <div class="col-header  text-left">
            <h1 class="my-0 text-blue font-weight-900">Area de Cobertura</h2>
          </div>

        </div>
      </div>
	  <div class="row mt50">
			
		<div class="col-sm-7 ">
		<div style="overflow:hidden;width: 100%;position: relative; border:none"><iframe style=" border:none" src="https://www.google.com/maps/d/embed?mid=1_oURIjjnKFQi6zxC_nUE_ot_fL4" width="100%" height="480"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"><small style="line-height: 1.8;font-size: 0px;background: #fff;"> <a href="https://googlemapsembed.net/">Google Maps Embed</a> </small></div><style>.nvs{position:relative;text-align:right;height:325px;width:643px;} #gmap_canvas img{max-width:none!important;background:none!important}</style></div>
		</div>
		<div class="col-sm-5 ">

			<div class="row mt50">
				<p style="font-size: 22px;">Caso sua residência ou empresa não esteja na cobertura da Cabangu Internet, entre em contato conosco, informando o seu endereço para que possamos estudar a viabilidade.</p>
			</div>  

			<form class="text-right">
				<input type="text" name="nome" placeholder="Nome: ">
				<input type="text" name="telefone" placeholder="Telefone: ">
				<input type="text" name="endereço" placeholder="Endereço: ">
				<button type="submit" name="enviar" class="btn btn-green submit-suporte text-white">Enviar</button>
			</form>

		</div>
	  </div>
    </div>
 </section>
<?php
get_footer();
