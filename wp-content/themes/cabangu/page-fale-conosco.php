<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cabangu
 */

get_header();

$mypost = get_page_by_path('Fale Conosco', '', 'page');
?>


<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/paralax.css">
<section class="fale-conosco">
	<div class=" ">
		<div class="col-sm ">
			<div class="col-header  text-center mt100">
             <h1 class=" text-blue font-weight-900">Envie sua Dúvida</h2>

			</div>

          <div class="col-body text-center text-gray header-envie-sua-duvida" style="font-size: 22px;">
            <p> <?php echo get_field('envie_sua_duvida',  $mypost->ID); ?>  </p>
          </div>
		</div>
	</div>
	<a href="" class="seta-absolut	"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta.png" alt=""></a>
</section>


<section class="formulario-fale-conosco">

    <div class="container">
		<div class="row ">
	             <h1 class="text-blue font-weight-900" >Fale Conosco</h1>
				 <p><?php echo get_field('fale_conosco',  $mypost->ID); ?>
             </p>
			</div>

		
	    	

		<div class="row ">
			<div class="col-sm-5 px-0 overflow text-left text-center-mobile">
			    <h4 class=" text-blue font-weight-900" >Suporte</h4>
			    <div class=" text-gray-light font-weight-bold" style="font-size: 40px">0800 031 3978</div>
			    <div class=" text-gray-light font-weight-100" style="font-size: 30px"><small>032</small> 3251-3978<span class="zap-ico"></span></div> 
			    <div class=" text-gray-light font-weight-100" style="font-size: 30px"><small>032</small> 9923-7775<span class="zap-ico"></span></div>
			 
            	</p>
			    <h4 class=" text-blue font-weight-900" >Comercial</h4>
			    <div class=" text-gray-light font-weight-100" style="font-size: 30px"><small>032</small> 9831-3998</div>
			 
            	</p>
			</div>

			<div class="card bg-blue col-sm-7 text-left">
				<p class="mt50"><?php echo get_field('texto_formulario',  $mypost->ID); ?></p>
				<!-- <form>
					
					<input type="text" name="nome" placeholder="Nome: ">
					<input type="text" name="email" placeholder="E-mail: ">
					<input type="text" name="telefone" placeholder="Telefone: ">
					<input type="text" name="cidade" placeholder="Cidade: ">
					<textarea  name="mensagem" placeholder="Mensagem: "></textarea>
					<div class="text-right">
						<button type="submit" name="enviar" class="btn btn-green submit-suporte text-white mt30">Enviar</button>
					</div>
				</form> -->
				<?php echo do_shortcode( '[contact-form-7 id="113" title="Contact form 1"]' ); ?>
			</div>
		</div>
	</div>
</section>

<section class="faixa-fale-conosco bg-blue">
	<div class="container">
		<div class="row">
			
			<div class="col-sm-6 text-left lado-dir">
				<div class="row">

					<div class="text-white col-sm-5 text-left text-center-mobile">
						<div class=" font-weight-bold " style="font-size: 30px; 
    line-height: 30px;">Seja um colaborador</div>
					</div>
					<div class=" col-sm-7 text-right	">
						<a href="https://forms.gle/vdU6JNqJefvJVF6o7" class="btn btn-green submit-suporte text-white"  style="margin:0">Trabalhe Conosco</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 text-left lado-esq">
				<div class="row">
					<div class=" text-white col-sm-5 text-left  text-center-mobile ">
						<div class=" font-weight-bold" style="font-size: 30px;    line-height: 30px;">Informações complementares</div>
						
					</div>
					<div class=" col-sm-7 text-right">
						<a href="<?php echo home_url(); ?>/contrato/contrato.pdf" class="btn btn-green submit-suporte text-white" style="margin:0">Contrato</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<?php
get_footer();
