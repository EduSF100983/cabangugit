<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- Ativa o modo de compatibilidade do IE -->  
    <meta charset="<?php bloginfo('charset'); ?>"><!-- Codificacao usada para escrever o documento -->  
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><!-- Necessario para layout resposivo -->
	  <link rel="profile" href="https://gmpg.org/xfn/11">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri();?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-shims.min.css" media="all" rel="stylesheet" id="font-awesome-5-kit-css">
    <link href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-font-face.min.css" media="all" rel="stylesheet" id="font-awesome-5-kit-css">
    <link href="https://kit-free.fontawesome.com/releases/latest/css/free.min.css" media="all" rel="stylesheet" id="font-awesome-5-kit-css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,423;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,423;1,500;1,600;1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Favicons -->
    <!--   <link rel="apple-touch-icon" href="assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c"> -->                           

    <!-- Custom styles for this template -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <link href="<?php echo get_template_directory_uri();?>/assets/css/zap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/assets/css/styles.css" rel="stylesheet">

    <?php wp_head(); ?>
  </head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

      <header> 
        <div class="text-right email-tel mt5" >
          
          <div class="container">  
            <span class="px-md-3"> <i class="far fa-envelope"></i> <span class="text-bold">suporte@cabanguinternet</span>.com.br </span>
            <span class=" text-blue text-bold"> <i class="fas fa-phone"></i> 0800 031 3978 </span>
          </div>
        </div>
        <div id="logo" class="my-0 mr-md-auto font-weight-normal text-center" style="    padding: 70px 0;">
          <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/assets/img/logo-g.png"></a>
        </div>
      </header>