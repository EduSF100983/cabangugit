<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cabangu
 */

get_header();
?>
<section class="sempre-conectado">
      
    <div class="mobile-content bg-blue">
      <div class="flex  ">
        <div class="col-sm" style="margin: 100px 0 0 0">
          <div class="col-header">
            <h1 class="text-white">Sempre <br> Conectado_</h1>
          </div>
          <div class="col-body">
            <p>Lorem ipsumdolor sit amet, consectetur <br> 
            adipisicing elit, sed do eiusmod tempor </p>  

            <a href="<?php echo home_url(); ?>/planos" class="btn btn-green " style="display: inline-block">Escolha seu plano</a>   
            <br>      
            <br>      
            <br>      
            <br>      
          </div>          
        </div>
      </div>
    </div>
   <div class="swiper-container">
    <div class="swiper-wrapper">


    <?php 
$args = array( 
'orderby' => 'id',
'order'   => 'ASC',
'post_type' => 'banner',
'posts_per_page' => -1
);
$the_query_banner = new WP_Query( $args );
?>

<?php if ( $the_query_banner->have_posts() ) : while ( $the_query_banner->have_posts() ) : $the_query_banner->the_post(); ?>
    <div class="swiper-slide" style="background-image: url('<?php echo get_field('imagem',  $the_query_banner->ID); ?>  ');">  
        <div class="container">  
          <div class="row mt200 pl70">
            <div class="col-sm">
              <div class="col-header">
                <h1 class="text-blue"><?php echo get_field('titulo',  $the_query_banner->ID); ?></h1>
              </div>
              <div class="col-body">
                    <?php echo get_field('html',  $the_query_banner->ID); ?>  
                <br>      
                <br>      
                <br>      
                <br>      
              </div>          
            </div>                
          </div>

        </div>
      </div>


      <?php endwhile; else: ?> <?php endif; ?> 
            <?php wp_reset_query(); ?>

       
    </div>
    <!-- Add Pagination -->
    
    
    <div class="swiper-pagination"></div>

  </div>
  
    </div>


  </section>



  <section class="vantangens">

  <div class="container">  
   <div class="row mt50 pl70">
     <div class="col-sm-6">
        <div class="col-header">
          <h2 class="text-blue">Vantagens de  <br> ser Cabangu</h2>
        </div>
        <div class="col-body">
          <p><?php
          $mypost = get_page_by_path('Home', '', 'page');
          echo get_field('vantagens_de_ser_cabangu',  $mypost->ID); ?></p>      
        </div>          
      </div>
      
      <div class="col-sm-1 ">
      </div>
      
      <div class="col-sm-5 ">
        <div class="col-body text-center">
            <div class="circulo-de-jogos relative">
                <div class="top active"><a class="jogo-button "></a></div>
                <div class="botton"><a class="monitor-button"></a></div>
                <div class="left"><a class="fone-button"></a></div>
                <div class="right"><a class="maleta-button"></a></div>
                <div class="bg-blue circulo-interno1 jogo"><h4><?php echo get_field('titulo_jogos',  $mypost->ID); ?></h4><p><?php echo get_field('texto_jogos',  $mypost->ID); ?></p></div>
                <div class="bg-blue circulo-interno1 monitor"><h4><?php echo get_field('titulo_monitor',  $mypost->ID); ?></h4><p><?php echo get_field('texto_monitor',  $mypost->ID); ?></p></div>
                <div class="bg-blue circulo-interno1 fone"><h4><?php echo get_field('titulo_fone',  $mypost->ID); ?></h4><p><?php echo get_field('texto_fone',  $mypost->ID); ?></p></div>
                <div class="bg-blue circulo-interno1 maleta"><h4><?php echo get_field('titulo_maleta',  $mypost->ID); ?></h4><p><?php echo get_field('texto_maleta',  $mypost->ID); ?></p></div>

            </div>
          <!-- <img src="<?php echo get_template_directory_uri();?>/assets/img/jogos.jpg" > -->
         
        </div>
      </div>
    </div>
  </div>
  </section>
  <section class="plano-pra-voce">
   <div class="container">  
   <div class="row">
     <div class="col-sm-12 text-center">
        <div class="col-header">
          <h2 class="text-blue ">Plano para você</h2>
          <h3 class="font-weight-normal"><?php echo get_field('plano_pra_voce',  $mypost->ID); ?></h3>
        </div>
        <div class="col-body">
          
          <div class="center"> 
                    <a  class="btnhalfl">Fibra</a>
                    <a  class="btnhalfr">Radio</a>
                    
                     <div class=" mobile-content">
                    <div class="planos-fibra ">
                      <div class="swiper-container2">
                        <div class="swiper-wrapper">
                          <?php 
                            $args = array( 
                            'orderby' => 'id',
                            'order'   => 'ASC',
                            'post_type' => 'planos_internet',
                            'meta_key'		=> 'tipo',
                            'meta_value'	=> 'Fibra',
                            'posts_per_page' => 3
                            );
                            $the_query = new WP_Query( $args );
                            ?>
                            <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                          <div class="swiper-slide  mt50 plano">
                              <div class="col-header">
                                <p> Plano de Fibra</p>
                                <h1 class="text-blue "><?php echo get_field('velocidade',  $the_query->ID) ?><small>mb</small></h1>
                              </div>
                              
                              <div class="col-body">   

                                    <a style="display:inline-block" href="<?php echo home_url(); ?>/planos" class="btn btn-green  text-white ">Saiba mais</a> <br> 
                                    <small> *Consulte viabilidade técnica</small>      
                              </div>
                            </div>

                          <?php endwhile; else: ?> <p>Não existe Planos para exibir!</p> <?php endif; ?> 
                          <?php wp_reset_query(); ?>
                         
                        </div>
                        <!-- Add Pagination -->   
                        <div class="swiper-pagination"></div>
                      </div>
                    </div>              
                    
                    <div class="planos-radio " >
                      <div class="swiper-container3">
                        <div class="swiper-wrapper">
                          <?php 
                            $args = array( 
                            'orderby' => 'id',
                            'order'   => 'ASC',
                            'post_type' => 'planos_internet',
                            'meta_key'		=> 'tipo',
                            'meta_value'	=> 'Radio',
                            'posts_per_page' => 3
                            );
                            $the_query = new WP_Query( $args );
                            ?>
                            <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                          <div class="swiper-slide  mt50 plano">
                              <div class="col-header">
                                <p> Plano de Radio</p>
                                <h1 class="text-blue "><?php echo get_field('velocidade',  $the_query->ID) ?><small>mb</small></h1>
                              </div>
                              
                              <div class="col-body">   

                                    <a style="display:inline-block" href="<?php echo home_url(); ?>/planos" class="btn btn-green  text-white ">Saiba mais</a> <br> 
                                    <small> *Consulte viabilidade técnica</small>      
                              </div>
                            </div>

                          <?php endwhile; else: ?> <p>Não existe Planos para exibir!</p> <?php endif; ?> 
                          <?php wp_reset_query(); ?>
                         
                        </div>
                        <!-- Add Pagination -->   
                        <div class="swiper-pagination"></div>
                      </div>
                    </div>

          </div>

                            <div class="desktop">
            <div class="row planos-fibra ">
              <?php 
              $args = array( 
              'orderby' => 'id',
              'order'   => 'ASC',
              'post_type' => 'planos_internet',
              'meta_key'		=> 'tipo',
              'meta_value'	=> 'Fibra',
              'posts_per_page' => 3
              );
              $the_query = new WP_Query( $args );
              ?>
              <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

             <div class="col-sm  mt50 plano">
                <div class="col-header">
                  <p> Plano de Fibra</p>
                  <h1 class="text-blue "><?php echo get_field('velocidade',  $the_query->ID) ?><small>mb</small></h1>
                </div>
                
                <div class="col-body">   

                      <a href="<?php echo home_url(); ?>/planos" class="btn btn-green  text-white ">Saiba mais</a> <br> 
                      <small> *Consulte viabilidade técnica</small>      
                </div>
              </div>

            <?php endwhile; else: ?> <p>Não existe Planos para exibir!</p> <?php endif; ?> 
            <?php wp_reset_query(); ?>
 
      
          </div>


            <div class="row planos-radio " style="display: none">
            <?php 
              $args = array( 
              'orderby' => 'id',
              'order'   => 'ASC',
              'post_type' => 'planos_internet',
              'meta_key'		=> 'tipo',
              'meta_value'	=> 'radio',
              'posts_per_page' => 3
              );
              $the_query = new WP_Query( $args );
              ?>
              <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

             <div class="col-sm  mt50 plano">
                <div class="col-header">
                  <p> Plano de Rádio</p>
                  <h1 class="text-blue "><?php echo get_field('velocidade',  $the_query->ID) ?><small>mb</small></h1>
                </div>
                
                <div class="col-body">   

                      <a href="<?php echo home_url(); ?>/planos" class="btn btn-green  text-white ">Saiba mais</a> <br> 
                      <small> *Consulte viabilidade técnica</small>      
                </div>
              </div>
            <?php endwhile; else: ?> <p>Não existe Planos para exibir!</p> <?php endif; ?> 
            <?php wp_reset_query(); ?>
            </div>
            </div>
          </div>      
        </div>          
      </div>
    </div>
    <div class="col-sm-12">
      <div class="text-center mt100">
                        <a href="<?php echo home_url(); ?>/planos" class="btn btn-green  text-white ">Todos os Planos</a> <br> </div>
      </div>
    </div>
    </section>

  <section class="fibra-pra-voce bg-blue mt50">
   <div class="container">  
    <div class="row">
      <div class="col-sm-1"></div>
      <div class="col-sm-5 text-right">
       <img src="<?php echo get_template_directory_uri();?>/assets/img/fribra-home.jpg" alt="" style="width:100%">        
     </div>
     <div class="col-sm-5 text-left p20 mt20">
        <div class="col-header">
          <h2 class="text-white ">Fibra <br> para você?</h2>
        </div>
        <div class="col-body">
          <p style="max-width: 350px "><?php echo get_field('fibra_pra_voce',  $mypost->ID); ?> </p>      
          
          <a href="<?php echo home_url(); ?>/suporte/#area-cobertura" class="btn btn-green">Consulte áreas Antendidas</a>
        </div>   
      </div>   
    
      <div class="col-sm-1"></div>
    
    </div>   

    <div class="row mt100">  
      <div class="col-sm-1"></div> 
       <div class="col-sm-4">
          <div class="col-header">
             <h2 class="text-white ">Dúvidas <br> Frequentes</h2>
          </div>        
        </div>
        <div class="col-sm-6 text-left">
        
          <div class="col-body">
           
          <ul class=" faq">  
            <?php 
$args = array( 
'orderby' => 'id',
'order'   => 'ASC',
'post_type' => 'faq',
'posts_per_page' => 4
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<li>  <div class="pergunta text-bold"><?php the_title() ;?>  <span class="sinal ">+</span></div> <div class="resposta" style="display: none;"><?php the_content() ?></div></li>
    
<?php endwhile; else: ?> <p>Não existe perguntas para exibir!</p> <?php endif; ?>

<?php wp_reset_query(); ?>
  </ul>  
            
           <a href="<?php echo home_url(); ?>/suporte#duvidas-frequentes" class="btn btn-green">Suporte</a>
          </div>
        </div>
      <div class="col-sm-1"></div>
      </div>
      
     </div>
     </div>
  </section>
<?php
get_footer();
