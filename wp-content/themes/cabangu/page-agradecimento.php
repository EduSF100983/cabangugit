<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cabangu
 */

get_header();

$mypost = get_page_by_path('Termos de Contratação', '', 'page');
?><link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/acabangu.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/paralax.css">
<section class="termos-de-contratacao">

    <div class="container">
	<div class="row ">
		<div class="col-sm ">
			<div class="col-header  text-center">
             <h1 class="my-0 text-blue font-weight-900">Termos de Contratação</h2>

			</div>

          <div class="col-body text-left text-gray mt50" style="font-size: 22px;">
            <p><?php echo get_field('termos_de_contratacao',  $mypost->ID); ?>  </p>
          </div>
		</div>
	</div>
	</div>
	<a href="#termos-de-contratacao-formulario" class="seta-absolut"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta.png" alt=""></a>
</section>


<section id="termos-de-contratacao-formulario" class="termos-de-contratacao-formulario">

    <div class="container">
		<div class="row relative">
			<div class="col-sm-12 overflow text-left mt100">
	        	<div class="linha-tracejada"></div>
            
			</div>
			<div class="col-sm-6 overflow text-center">
	        	<span class="circulo bg-green">01</span>
            
			</div>
			<div class="col-sm-6 overflow text-center">
	        	<span class="circulo bg-white text-blue">02</span>
            
			</div>
		</div>	

		<div class="row mt100" style="padding-bottom: 50px">
		<h1 style="padding-bottom: 100px " class="my-0 text-blue font-weight-900">Obrigado, nossa equipe irá entrar em Contato!  </h1>
		
</section>
<section class="regras" style="display:none">

    <div class="container">

    	<h1 class="my-0 text-blue font-weight-900">Os Equipamentos Instalados
				são de Propriedade da Contratada </h1>
		<div class="row ">
			<div class="col-sm-6 ">
				<div class="col-header  text-left mt30">
            		<?php echo get_field('equipamentos_instalados_1',  $mypost->ID); ?> 
				</div>
          	</div>
			<div class="col-sm-6 ">
				<div class="col-header  text-left mt30">
            		<?php echo get_field('equipamentos_instalados_2',  $mypost->ID); ?> 
				</div>
          	</div>
		</div>


    <div class="container mt100 ">

	</div>
    	<h1 class="my-0 text-blue font-weight-900 ">Termos de Permanência </h1>
		<div class="row ">
			<div class="col-sm-6 ">
				<div class="col-header  text-left mt30">
            		<?php echo get_field('equipamentos_instalados_3',  $mypost->ID); ?> <br><br>

				</div>
          	</div>
			<div class="col-sm-6 ">
				<div class="col-header  text-left mt30">
				<?php echo get_field('equipamentos_instalados_4',  $mypost->ID); ?>



				</div>
          	</div>
		</div>
	</div>
	
</section>

<section class="baixe-agora bg-blue">
	<div class="container">
		<div class="row">
			
			<div class="col-sm-12 text-center flex" style="padding: 50px; flex-direction:row;
    align-items: center;justify-content: center;">
						<div class="my-0 font-weight-bold" style="font-size: 30px;    line-height: 30px; 
    align-self: center; margin-right:20px;">Baixe seu contrato aqui:  </div>
						<a href="http://cabanguinternet.com.br/contrato/contrato.pdf" class="btn btn-green submit-suporte text-white"  style="margin:0;align-self: center;">Baixar Agora</a>
			</div>

		
		</div>
	</div>

</section>
<?php
get_footer();
