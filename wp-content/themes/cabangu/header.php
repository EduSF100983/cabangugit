<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cabangu
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Rodrigo Gonçalves, Eduardo Ferreira">
  <meta name="generator" content="">
  <title>Cabangu</title>


<!-- Bootstrap core CSS -->
<link href="<?php echo get_template_directory_uri();?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-shims.min.css" media="all" rel="stylesheet" id="font-awesome-5-kit-css">
<link href="https://kit-free.fontawesome.com/releases/latest/css/free-v4-font-face.min.css" media="all" rel="stylesheet" id="font-awesome-5-kit-css">
<link href="https://kit-free.fontawesome.com/releases/latest/css/free.min.css" media="all" rel="stylesheet" id="font-awesome-5-kit-css">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,423;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,423;1,500;1,600;1,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <!-- Favicons -->
<!--   <link rel="apple-touch-icon" href="assets/img/favicons/apple-touch-icon.png" sizes="180x180">
  <link rel="icon" href="assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
  <link rel="icon" href="assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
  <link rel="manifest" href="assets/img/favicons/manifest.json">
  <link rel="mask-icon" href="assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
  <link rel="icon" href="assets/img/favicons/favicon.ico">
  <meta name="msapplication-config" content="assets/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#563d7c"> -->

  <!-- Custom styles for this template -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
  <link href="<?php echo get_template_directory_uri();?>/assets/css/zap.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri();?>/assets/css/styles.css" rel="stylesheet">
 
</head>
<?php wp_body_open(); ?>


<?php
 if ($pagename == 'planos'){
    $link_planos = 'active';
    $link_a_cabangu = '';
    $link_fale_conosco = '';
 }else if  ($pagename == 'a-cabangu'){
  $link_planos = '';
  $link_a_cabangu = 'active';
  $link_fale_conosco = '';
}else if  ($pagename == 'fale-conosco'){
  $link_planos = '';
  $link_a_cabangu = '';
  $link_fale_conosco = 'active';
}

?>
      <header> 
        <div class="text-right email-tel mt5" >
          
          <div class="container">  
            <span class="px-md-3"> <i class="far fa-envelope"></i> <span class="text-bold">suporte@cabanguinternet</span>.com.br </span>
            <span class=" text-blue text-bold"> <i class="fas fa-phone"></i> 0800 031 3978 </span>
          </div>
        </div>
        <div class="container">
        <div class="d-flex flex-column flex-md-row align-items-center mt30">
        
          <div id="logo" class="my-0 mr-md-auto font-weight-normal">
       
            <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/assets/img/logo.png"></a>
          </div>
          <nav class="">
            <a class = "<?php echo $link_planos; ?>" href="<?php echo home_url(); ?>/planos">Planos</a>
            <a class = "<?php echo $link_a_cabangu; ?>"  href="<?php echo home_url(); ?>/a-cabangu">A Cabangu</a>
            <a class = "<?php echo $link_fale_conosco; ?>"  href="<?php echo home_url(); ?>/fale-conosco">Fale conosco</a>
            <a class="btn-mode" href="<?php echo home_url(); ?>/suporte">Suporte</a>
          </nav>
         </div>
        </div>
      </header>

<!--Navbar-->
<nav class="navbar navbar-light light-blue lighten-4 mobile-content">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="<?php echo home_url(); ?>"><span class="logo-img normal"></span></a>

  <!-- Collapse button -->
  <button class="navbar-toggler toggler-example" type="button"><span class="dark-blue-text"><i
        class="ico-menu hamb"></i></span></button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent1">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="<?php echo $link_planos; ?>" href="<?php echo home_url(); ?>/planos">Planos</a>
      </li>
      <li class="nav-item">
            <a class = "<?php echo $link_a_cabangu; ?>"  href="<?php echo home_url(); ?>/a-cabangu">A Cabangu</a>
      </li>
      <li class="nav-item">
            <a class = "<?php echo $link_fale_conosco; ?>"  href="<?php echo home_url(); ?>/fale-conosco">Fale conosco</a>
      </li>
      <li class="nav-item">
            <a class = "btn-mode <?php echo $link_fale_conosco; ?>"  href="<?php echo home_url(); ?>/suporte">Suporte</a>
      </li>
    </ul>
    <!-- Links -->
    <div class="rede-social" >

        <h5 class="text-white text-left"> Siga-nos!</h5>

      <a class="inline" href="https://www.facebook.com/cabanguinternet"><img src="<?php echo get_template_directory_uri();?>/assets/img/facebook-ico.png"></a>
      <a class="inline" href="https://www.instagram.com/cabanguinternet/"><img src="<?php echo get_template_directory_uri();?>/assets/img/insta-ico.png"></a>
    </div>
  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->