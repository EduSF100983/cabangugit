<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cabangu
 */

get_header();
$hoje = date('Y-m-d');
$mypost = get_page_by_path('Planos', '', 'page');
?>


<section class="melhores-planos  bg-blue">

<div class="container container bg-gray-light">
  <div class="row ">
    <div class="col-sm mt100 ">
      <div class="col-header  text-center">
        <h2 class="my-0 text-blue">Melhores Planos <br> da Melhor Internet</h2>
      </div>
      <div class="col-body text-center text-gray mt50" style="font-size: 22px;">
        <p><?php echo get_field('melhores_planos_da_melhor_internet',  $mypost->ID); ?> </p>
      </div>
    </div>
  </div>
</div>

<div class=" ">
  <div class="col-sm mt200 ">
    <div class="col-header  text-center">
      <h2 class="my-0 text-white">Escolha a sua cidade</h2>
    </div>
    <div class="col-body text-center mt50">
      <div class="div-select-fake">
        <div class="text">Selecione uma cidade</div>
        <div class="options">
          <a href="juiz-de-fora">Juiz de Fora</a>
          <a href="Santos-Dummond">Santos Dummond</a>
        </select>
      </div>
    </div>
  </div>
</div>
</div>
</section>

<section class="melhores-planos-fibra">
<div class="container">
  <div class="row pl70">
    <div class="col-sm text-center">
      <div class="col-header">
        <h3 class="font-weight-normal"><?php echo get_field('melhores_planos_2',  $mypost->ID); ?> </h3>
      </div>
      <div class="col-body">

        <div class="center">
          <div class="botoes">
            <a class="btnhalfl">Fibra</a>
            <a class="btnhalfm">Corporativo</a>
            <a class="btnhalfr">Radio</a>
          </div>
          
          
          
          <div class="row planos-fibra ">
          <div class="col-sm-12 text-left mt40">
            <h1 class="text-blue">Planos Fibra Óptica</h1>
          </div>
          <?php 
              $args = array( 
              'orderby' => 'id',
              'order'   => 'ASC',
              'post_type' => 'planos_internet',
              'meta_key'		=> 'tipo',
              'meta_value'	=> 'fibra',
              'posts_per_page' => -1
              );
              $the_query = new WP_Query( $args );
              ?>
              <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


              <div class="col-sm-4  mt50">
              <div class="plano-interno">
                <div class="col-header">
                  <p> Plano de Fibra</p>
                  <h1 class="my-0 text-blue "><?php echo get_field('velocidade',  $the_query->ID) ?><small>Mb</small></h1>
                  <p> Wi-fi grátis</p>

                  <h3 class="my-0 text-gray "><small>R$</small><?php echo get_field('preco',  $the_query->ID) ?>,<small>90</small></h3>
                </div>

                <div class="col-body">

                  <a href="<?php echo home_url(); ?>/termos-de-contratacao?preco=<?php echo get_field('preco',  $the_query->ID) ?>&velocidade=<?php echo get_field('velocidade',  $the_query->ID) ?>&dia=<?php echo $hoje?>&ativar=1" class="btn btn-green  text-white small">Assinar Já</a> 
                  <small class="consulte"> *Consulte viabilidade técnica</small>

                </div>


                <div class="col-body text-left bg-f7 detalhes">
                 
                    Dowlnoad até <?php echo get_field('download',  $the_query->ID) ?>Mbps <br>
                    Upload até <?php echo get_field('upload',  $the_query->ID) ?>Mbps<br><br>

                    Instalação e Suporte Grátis
                </div>
              </div>
            </div>
            <?php endwhile; else: ?> <p>Não existe Planos pra exibir!</p> <?php endif; ?> 
            <?php wp_reset_query(); ?>
          </div>
          <div class="row planos-radio " style="display: none">
          
          <div class="col-sm-12 text-left mt40">
            <h1 class="text-blue">Planos de Rádio</h1>
          </div>
          <?php 
              $args = array( 
              'orderby' => 'id',
              'order'   => 'ASC',
              'post_type' => 'planos_internet',
              'meta_key'		=> 'tipo',
              'meta_value'	=> 'radio',
              'posts_per_page' => -1
              );
              $the_query = new WP_Query( $args );
              ?>
              <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


              <div class="col-sm-4  mt50">
              <div class="plano-interno">
                <div class="col-header">
                  <p> Plano de Rádio</p>
                  <h1 class="my-0 text-blue "><?php echo get_field('velocidade',  $the_query->ID) ?><small>Mb</small></h1>
                  <p> Wi-fi grátis</p>

                  <h3 class="my-0 text-gray "><small>R$</small><?php echo get_field('preco',  $the_query->ID) ?>,<small>90</small></h3>
                </div>

                <div class="col-body">

                  <a href="<?php echo home_url(); ?>/termos-de-contratacao?preco=<?php echo get_field('preco',  $the_query->ID) ?>&velocidade=<?php echo get_field('velocidade',  $the_query->ID) ?>&dia=<?php echo $hoje?>&ativar=1" class="btn btn-green  text-white small">Assinar Já</a> 
                  <small class="consulte"> *Consulte viabilidade técnica</small>

                </div>


                <div class="col-body text-left bg-f7 detalhes">
                
                    Dowlnoad até <?php echo get_field('download',  $the_query->ID) ?>Mbps <br>
                    Upload até <?php echo get_field('upload',  $the_query->ID) ?>Mbps<br><br>

                    Instalação e Suporte Grátis
                </div>
              </div>
            </div>
            <?php endwhile; else: ?> <p>Não existe Planos pra exibir!</p> <?php endif; ?> 
            <?php wp_reset_query(); ?>
          </div>
          <div class="row planos-corporativo " style="display: none">

          <div class="col-sm-12 text-left mt40">
            <h1 class="text-blue">Planos Corporativos</h1>
          </div>
          <?php 
              $args = array( 
              'orderby' => 'id',
              'order'   => 'ASC',
              'post_type' => 'planos_internet',
              'meta_key'		=> 'tipo',
              'meta_value'	=> 'corp',
              'posts_per_page' => -1
              );
              $the_query = new WP_Query( $args );
              ?>
              <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


              <div class="col-sm-4  mt50">
              <div class="plano-interno">
                <div class="col-header">
                  <p> Plano de Fibra</p>
                  <h1 class="my-0 text-blue "><?php echo get_field('velocidade',  $the_query->ID) ?><small>Mb</small></h1>
                  <p> Wi-fi grátis</p>

                  <h3 class="my-0 text-gray "><small>R$</small><?php echo get_field('preco',  $the_query->ID) ?>,<small>90</small></h3>
                </div>

                <div class="col-body">

                  <a href="<?php echo home_url(); ?>/termos-de-contratacao?preco=<?php echo get_field('preco',  $the_query->ID) ?>&velocidade=<?php echo get_field('velocidade',  $the_query->ID) ?>&dia=<?php echo $hoje?>&ativar=1" class="btn btn-green  text-white small">Assinar Já</a> 
                  <small class="consulte"> *Consulte viabilidade técnica</small>

                </div>


                <div class="col-body text-left bg-f7 detalhes">
          
                    Dowlnoad até <?php echo get_field('download',  $the_query->ID) ?>Mbps <br>
                    Upload até <?php echo get_field('upload',  $the_query->ID) ?>Mbps<br><br>

                    Instalação e Suporte Grátis
                </div>
              </div>
            </div>
            <?php endwhile; else: ?> <p>Não existe Planos pra exibir!</p> <?php endif; ?> 
            <?php wp_reset_query(); ?>
          </div>
          </div>


        </div>
      </div>
    </div>
  </div>
</section>

<section class="vantagens-cabangu bg-blue">
<div class="container">
  <div class="row">
    
    <div class="col-sm text-left  mt20">
      <div class="col-header">
        <h2 class="my-0 text-white ">Vantagens Cabangu</h2>
      </div>
      <div class="row">
        <div class="col-sm text-center ">
          <div class="iconeverde text-center mt50 "><img src="<?php echo get_template_directory_uri();?>/assets/img/fone.png" alt=""></div>
          <h4>Atendimento 24h</h4>
          <p style="max-width: 350px "><?php echo get_field('vantagens_cabangu_1',  $mypost->ID); ?>  </p>
        </div>
        <div class="col-sm text-center ">
          <div class="iconeverde text-center mt50 "><img src="<?php echo get_template_directory_uri();?>/assets/img/radio.png" alt=""></div>
          <h4>Suporte Local</h4>
          <p style="max-width: 350px "><?php echo get_field('vantagens_cabangu_2',  $mypost->ID); ?>  </p>
        </div>
        <div class="col-sm text-center ">
          <div class="iconeverde text-center mt50 "><img src="<?php echo get_template_directory_uri();?>/assets/img/maos.png" alt=""></div>
          <h4>Internet Instável</h4>
          <p style="max-width: 350px "><?php echo get_field('vantagens_cabangu_3',  $mypost->ID); ?>  </p>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</section>
<?php
get_footer();
