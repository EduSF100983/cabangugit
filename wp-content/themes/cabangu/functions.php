<?php

// Chamar TAG title
function titulo_pag() {
    add_theme_support('title-tag');
}
add_action('after_setup_theme', 'titulo_pag');

// Previnir erro na TAG title
if (!function_exists('_wp_render_title_tag')) {
    function titulo_pag_render() {
        ?>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <?php
    }
    add_action('wp_head', 'titulo_pag_render');
}

// Barra admin
show_admin_bar(false);

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;
  $pagetitle = get_the_title($post_id);
    remove_post_type_support('page', 'editor');
}


//remove o css do contact form
function rjs_lwp_contactform_css_js() {
    global $post;
    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'contact-form-7') ) {
         wp_enqueue_style('contact-form-7');

    }else{
        wp_dequeue_style( 'contact-form-7' );
    }
}
add_action( 'wp_enqueue_scripts', 'rjs_lwp_contactform_css_js');

//Include

function show_post($path) {
	$post = get_page_by_path($path);
	$content = apply_filters('the_content', $post->post_content);
	echo $content;
  }
// Bootstrap Navbar Walker
//require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

// Registrar menu
register_nav_menus( array(
    'principal' => __('Menu principal', 'menu')
));

// Thumbnail
add_theme_support( 'post-thumbnails' );

// Banner da HOME
function create_post_type(){
    register_post_type('banner',
    // Definir opções
    array(
        'labels'=> array(
            'name' => __('Banner'),
            'singular_name' => __('Banner')
        ),
        'supports' => array(
            'title', 'editor', 'thumbnail'
        ),
        'public' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-images-alt2',
        'rewrite' => array('slug' => 'banner'),
    ));
}
// Iniciar o tipo de post
add_action('init', 'create_post_type');