<footer class="">

<div class="container">  

  <div class=""> 
    <div class="row"> 
      <div class="col-sm-4 ">
      <a href="/"><img src="<?php echo get_template_directory_uri();?>/assets/img/logo-footer.png"></a>
      </div>
      <div class="col-sm-8 ">
      <div class="row"> 

        <div class="col-sm-4 ">
        <h5 class="text-blue"> Juiz de Fora</h5>
        <p> Av. Pres. Juscelino Kubitschek, <br>
        4815 - Nova Era</p> 

        <h5 class="text-blue"> Santos Dummond</h5>
        <p> Rua Antônio Abud, <br> 
          31 - Centro</p>


        </div>
        <div class="col-sm-4 ">

        <h5 class="text-blue"> Mais</h5>
        <a href="https://forms.gle/vdU6JNqJefvJVF6o7">Trabalhe Conosco</a>    
        <a href="<?php echo home_url(); ?>/Suporte">Dúvidas Frequentes</a>    
        <a href="<?php echo home_url(); ?>/fale-conosco">Fale Conosco</a>    
        <a href="<?php echo home_url(); ?>/contrato/contrato.pdf">Contrato</a>      
        </div>
        <div class="col-sm-4 ">

        <h5 class="text-blue"> Siga-nos</h5>
        
        <a class="inline" href="https://www.facebook.com/cabanguinternet"><img src="<?php echo get_template_directory_uri();?>/assets/img/facebook-ico.png"></a>
        <a class="inline" href="https://www.instagram.com/cabanguinternet/"><img src="<?php echo get_template_directory_uri();?>/assets/img/insta-ico.png"></a>
        </div>

      </div>
      </div>

    </div>
  </div>
</div>
</footer>
<div class="whatsapp fixed ">
                  <div class="col-body">

                    <div class="wc_whatsapp_app right">
                      <a href="https://wa.me/5521979282585" target="_blank" class="wc_whatsapp" >
                        <span class="wc_whatsapp_primary">
                          <p> <span class="text-bold">Olá! </span> Caso você tenha alguma dúvida, fale comigo.
                          Estou aqui para te ajudar!</p>
                        </span>
                      </a>
                    </div>
                   
</div>
</div>
<script src="https://use.fontawesome.com/7973dbb7d8.js"></script>
<script
src="https://code.jquery.com/jquery-1.11.3.min.js"
integrity="sha256-7LkWEzqTdpEfELxcZZlS6wAx5Ff13zZ83lYO2/ujj7g="
crossorigin="anonymous"></script> <!-- remova se você já tiver o jquery sendo carregado em sua página -->
<script
src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>


<?php 
if (is_page('home')){
  ?>
  
<script type='text/javascript'>

$(document).ready(function () {

$(".circulo-de-jogos a").hover(function(){
  $(".circulo-de-jogos a").parent().removeClass("active");
  $(this).parent().addClass("active");

$(".jogo").hide();
$(".monitor").hide();
$(".fone").hide();
$(".maleta").hide();

if($(this).hasClass("jogo-button")){
  $(".jogo").show();
  console.log('jogo');
}
if($(this).hasClass("monitor-button")){
  $(".monitor").show();
  console.log('jogo');
}
if($(this).hasClass("fone-button")){
  $(".fone").show();
}
if($(this).hasClass("maleta-button")){
  $(".maleta").show();
}
});




});
</script>
<?php
};
?>
<?php 
if (is_page('termos-de-contratacao')){
  ?>
  
<script type='text/javascript'>

$(document).ready(function () {
  
$('body').removeClass('corrigir_altura');

<?php

if(isset($_GET['ativar']) &&$_GET ['ativar']==1){
?>  

    if($('form').hasClass('invalid')){
      $("#continuar").hide();
      $(".form2").show();
      $(".regras").show();
      $(".concordar-termos").show();
    }
    // $("#continuar").hide();
    // $(".form2").show();
    // $(".regras").show();
    // $(".concordar-termos").show();
    $('input[name*="valormensal"]').val('<?php echo $_GET ['preco']?>,90'); 
    $('input[name*="plano"]').val('Plano <?php echo $_GET ['velocidade']?> MB');
    // var today = new Date().toISOString().split('T')[0];
    $('input[name*="datacontrat"]').val('<?php echo $_GET ['dia']?>'); 
    $('input[name*="custoimplant"]').val('100,00');
   
<?php
}

?>

var nome = $('input[name*="nome"]');
var nascimento = $('input[name*="nascimento"]');
var cpfcnpj = $('input[name*="cpfcnpj"]');
var nomemae = $('input[name*="nomemae"]');
var rg = $('input[name*="rg"]');
var endereco = $('input[name*="endereco"]');
var cidade = $('select[name*="cidade"]');
var tel1 = $('input[name*="tel1"]');
var tel2 = $('input[name*="tel2"]');
var email = $('input[name*="email"]');
var cep = $('input[name*="cep"]');
$("#continuar").click(function(){
  if(nome.val()!="" && nascimento.val()!="" && cpfcnpj.val()!="" && nomemae.val()!="" && cep.val()!="" && rg.val()!="" && endereco.val()!=""  && nome.val()!="" && tel1.val()!="" && email.val()!="" ){

    
    $(".form2").show();
    $(".regras").show();
    $(".concordar-termos").show();
    // alert('foi');
  }
});


});
</script>
<?php
};
?>
<script type='text/javascript'>

$(document).ready(function () {

 setTimeout(() => {
  $('.wc_whatsapp_primary').fadeOut(400);
 }, 5000);

 setTimeout(() => {
  $('.planos-radio').hide();
 }, 500);
}); 
//controla botão de fechar da mensagem primária
$('.wc_whatsapp_primary .close').click(function () {
$('.wc_whatsapp_primary').fadeOut(400);
return false;
});

$('.navbar-toggler').click(function(){
    if($('.navbar.mobile-content').hasClass('menu-open')){
      $('.navbar.mobile-content').removeClass('menu-open');
      $('.ico-menu').addClass('hamb');
      $('.ico-menu').removeClass('fechar');
      $('.logo-img').addClass('normal');
      $('.logo-img').removeClass('branco');
    }else{
      $('.navbar.mobile-content').addClass('menu-open');
      $('.ico-menu').removeClass('hamb');
      $('.ico-menu').addClass('fechar');
      $('.logo-img').removeClass('normal');
      $('.logo-img').addClass('branco');
    }
  $('#navbarSupportedContent1').slideToggle();
});


</script>
<script type='text/javascript'>

  $(window).bind('scroll', function () {
    console.log($(window).scrollTop())
    if ($(window).scrollTop() > 120 && $(window).scrollTop() < 700  ) {
        $('header').addClass('hide');
        $('header').removeClass('fixed');
        
        $('body').addClass('corrigir_altura');
        
    }else if($(window).scrollTop() > 700 ) {
      
      $('header').addClass('fixed');
      $('header').removeClass('hide');
    } else {
        $('header').removeClass('fixed');
        $('header').removeClass('hide');
        $('body').removeClass('corrigir_altura');
    }
});

$(document).ready(function () {

  if ($(window).scrollTop() > 120 ) {
         $('body').addClass('corrigir_altura');
        
  }
    
  <?php 
if (is_page('home')){
  ?>
  
  $('.btnhalfl').click(function(){
      console.log('ligar fibra');

       $('.btnhalfl').css('backgroundColor', '#95c11e');
       $('.btnhalfr').css('backgroundColor', '#b1b1b1');

       $('.planos-fibra').show();
       $('.planos-radio').hide();
  });

    $('.btnhalfr').click(function(){
      console.log('ligar radio')

       $('.btnhalfl').css('backgroundColor', '#b1b1b1');
       $('.btnhalfr').css('backgroundColor', '#95c11e');

       $('.planos-radio').show();
       $('.planos-fibra').hide();

  });


  <?php 
  }else if(is_page('planos')){ 
  ?>


   
  $('.btnhalfl').click(function () {
        console.log('ligar fibra');

        $('.btnhalfl').css('backgroundColor', '#95c11e');
        $('.btnhalfr').css('backgroundColor', '#b1b1b1');
        $('.btnhalfm').css('backgroundColor', '#b1b1b1');

        $('.planos-fibra').show();
        $('.planos-radio').hide();
        $('.planos-corporativo').hide();
      });

      $('.btnhalfr').click(function () {
        console.log('ligar radio')

        $('.btnhalfl').css('backgroundColor', '#b1b1b1');
        $('.btnhalfr').css('backgroundColor', '#95c11e');
        $('.btnhalfm').css('backgroundColor', '#b1b1b1');

        $('.planos-radio').show();
        $('.planos-fibra').hide();
        $('.planos-corporativo').hide();

      });

      $('.btnhalfm').click(function () {
        console.log('ligar corporativo')

        $('.btnhalfl').css('backgroundColor', '#b1b1b1');
        $('.btnhalfr').css('backgroundColor', '#b1b1b1');
        $('.btnhalfm').css('backgroundColor', '#95c11e');

        $('.planos-radio').hide();
        $('.planos-fibra').hide();
        $('.planos-corporativo').show();

      });


      <?php 
  }
  ?>

  $('.options a').click(function(event){
    event.preventDefault();
    console.log($(this).text());
     $('.div-select-fake .text').text($(this).text());
  });

    $('.faq li').click(function(){
       
    console.log($(this).text());

    if($(this).find('.sinal').text() == '+'){      
      $('.resposta').slideUp();
      $('.sinal').text('+'); 
      $(this).find('.sinal').text('-');
       $(this).find('.sinal').parent().parent().find('.resposta').slideDown();
    }else if($(this).find('.sinal').text() == '-'){
      $(this).find('.sinal').text('+'); 
      $('.resposta').slideUp();
    }
    });
  //initialize swiper when document ready
  var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'vertical',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    loop: true
  });


    //initialize swiper when document ready
    var mySwiper2 = new Swiper ('.swiper-container2', {
    // Optional parameters
    
    direction: 'horizontal',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    loop: true
  });

//initialize swiper when document ready
var mySwiper3 = new Swiper ('.swiper-container3', {
// Optional parameters

direction: 'horizontal',
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
loop: true
});



});

</script>

</body>
</html>
