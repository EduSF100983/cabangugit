<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cabangu
 */

get_header();

$mypost = get_page_by_path('A Cabangu', '', 'page');
?>



<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/paralax.css">
<section class="a-cabangu parallax-a-cabangu">
	<div class="row ">
		<div class="col-sm ">
			<div class="col-header  text-center ">
             <h1 class="text-white font-weight-900">Conheça a Cambangu</h2>

			</div>

          <div class="col-body text-center text-white mt50" style="font-size: 22px;">
            <p><?php echo get_field('conheca_a_cambangu',  $mypost->ID); ?></p>
          </div>
		</div>
	</div>
  
	<a href="" class="seta-absolut"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta.png" alt=""></a>
</section>


<section class="desde">

    <div class="container">
		<div class="row ">
			<div class="col-sm-4 overflow text-center desde-1996">
	             <h1 class="text-blue font-weight-900" >DESDE <br> 1996</h2>
				<img  class="image-1-left" src="<?php echo get_field('imagem_esquerda',  $mypost->ID); ?>">
			</div>

			<div class="col-sm-4 text-center">
				<img src="<?php echo get_field('imagem_centro',  $mypost->ID); ?>">

			</div>

			<div class="col-sm-4 overflow text-right text-center">
				
				<img  class="image-2-right" src="<?php echo get_field('imagem_direita',  $mypost->ID); ?>">
				<p class="text-left" style="margin-top: 20px;"><?php echo get_field('historia',  $mypost->ID); ?></p>
			</div>
		</div>
	</div>
</section>

<section class="mapa">

    <div class="container">
		<div class="row ">
			<div class="col-sm-7 overflow text-center">
				<img src="<?php echo get_template_directory_uri();?>/assets/img/mapa-pg-acabangu.png">
			</div>

			<div class="col-sm-5 text-left">
				
	             <h1 class="my-0 text-blue font-weight-900" style="margin-top: 50px !important">Conectando a Zona da Mata com o Mundo</h2>
				<p style="margin-top: 20px;"><?php echo get_field('conectando_a_zona_da_mata_com_o_mundo',  $mypost->ID); ?></p>

				<a href="<?php echo home_url(); ?>/suporte/#area-cobertura" class="btn btn-green submit-suporte text-white">Consulte Áreas Atendidas </a>
			</div>
		</div>
	</div>
</section>
<?php
get_footer();
